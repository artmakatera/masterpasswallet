import { createStackNavigator } from 'react-navigation';

import StartScreen from '../screens/StartScreen';
import SMSCodeScreen from '../screens/SMSCodeScreen';

const Navigator = createStackNavigator(
  {
    Home: StartScreen,
    SmsScreen: SMSCodeScreen
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

export default Navigator;
