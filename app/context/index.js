/* @flow */
import { createContext } from 'react';

type LoaderContextInterface = {
  isLoading: boolean,
  toggleLoading: Function
};
export const LoaderContext = createContext<LoaderContextInterface>({
  isLoading: false,
  toggleLoading: () => {}
});
