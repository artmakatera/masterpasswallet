/* @flow */

import React, { Component } from 'react';

import { StyleProvider, View } from 'native-base';
import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';

import Spinner from './components/Spinner';
import { LoaderContext } from './context';

import Navigator from './navigation';

type Props = {};

type State = {
  isLoading: boolean,
  toggleLoading: Function
};

export default class App extends Component<Props, State> {
  toggleLoading: Function;

  constructor(props: Props) {
    super(props);

    this.toggleLoading = (isLoading: boolean) => {
      this.setState({ isLoading });
    };

    this.state = {
      isLoading: false,
      toggleLoading: this.toggleLoading
    };
  }

  render() {
    return (
      <StyleProvider style={getTheme(platform)}>
        <>
          <Spinner condition={this.state.isLoading} />
          <LoaderContext.Provider value={this.state}>
            <Navigator />
          </LoaderContext.Provider>
        </>
      </StyleProvider>
    );
  }
}
