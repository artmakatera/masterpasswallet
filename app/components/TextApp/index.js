/* @flow */
import * as React from 'react';
import { Text } from 'native-base';

const TextApp: React.StatelessFunctionalComponent<{
  style?: Object,
  children: React.Node
}> = ({ style = {}, children }) => <Text style={{ ...style }}>{children}</Text>;

export default TextApp;
