import * as React from 'react';
import { View } from 'native-base';
import TimerCountdown from 'react-native-timer-countdown';
import Image from 'react-native-remote-svg';

import styles from './styles';

type Props = {
  onTimeElapsed: Function,
  initialSecondsRemaining: number
};

const Timer: React.StatelessFunctionalComponent<Props> = ({
  onTimeElapsed,
  initialSecondsRemaining
}) => (
  <View style={styles.timerContainer}>
    <TimerCountdown
      initialSecondsRemaining={initialSecondsRemaining}
      onTimeElapsed={onTimeElapsed}
      allowFontScaling={true}
      style={styles.timer}
    />
    <Image
      style={styles.timerImage}
      source={require('../../assets/images/timer.svg')}
    />
  </View>
);

export default Timer;
