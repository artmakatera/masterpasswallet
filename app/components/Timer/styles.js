import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  timerContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  timer: {
    fontSize: 15,
    color: '#FDDA24'
  },
  timerImage: {
    width: 16,
    height: 14,
    paddingRight: 3
  }
});
