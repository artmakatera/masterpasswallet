/* @flow */
import * as React from 'react';
import { View, Text } from 'native-base';
import Modal from 'react-native-modal';

import ButtonApp from '../ButtonApp';

import styles from './styles';

type ModalWindowProps = {
  isVisible: boolean,
  closeModal: Function
};

const ModalWindow: React.StatelessFunctionalComponent<ModalWindowProps> = ({
  isVisible,
  closeModal
}) => (
  <View>
    <Modal backdropOpacity={0.5} isVisible={isVisible}>
      <View style={styles.modalContainer}>
        <View style={styles.modalBox}>
          <Text style={styles.modalHeader}>Гаманець успішно з’єднано!</Text>
          <Text style={styles.modalDescription}>
            Тепер ви можете швидко поповнити рахунок своєю банківськую картою
            собі або близьким та друзям!
          </Text>
          <ButtonApp
            textCustomStyle={styles.modalBtnText}
            warning
            onPress={closeModal}
            style={styles.modalBtn}
          >
            Гаразд
          </ButtonApp>
        </View>
      </View>
    </Modal>
  </View>
);

export default ModalWindow;
