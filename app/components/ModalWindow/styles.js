import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalBox: {
    width: 335,
    height: 198,
    backgroundColor: '#fff',
    padding: 24
  },
  modalHeader: {
    color: '#3B4952',
    fontSize: 17,
    fontWeight: '900'
  },
  modalDescription: {
    color: '#90979B',
    fontSize: 13,
    paddingTop: 14
  },
  modalBtn: {
    width: 160,
    borderRadius: 50,
    alignSelf: 'center',
    marginTop: 24
  },
  modalBtnText: {
    color: '#3B4952',
    fontWeight: '700'
  }
});
