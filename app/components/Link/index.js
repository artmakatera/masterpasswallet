/* @flow */
import * as React from 'react';
import { View, Badge, Text, Icon, Button } from 'native-base';

import styles from './styles';

const Link: React.StatelessFunctionalComponent<{
  style?: Object,
  disabled?: boolean,
  onPress?: Function,
  children: React.Node
}> = props => (
  <Button
    block
   
    transparent
    onPress={props.onPress}
    disabled={props.disabled}
  >
  <View style={styles.badgeContainer}>
    <View style={styles.badgeStyle}>
      <Icon type="MaterialIcons" name="chevron-right" style={styles.linkIcon} />
    </View>
    <Text
      style={{
        fontSize: 15,
        paddingHorizontal: 0,
        color: '#fff',
        opacity: props.disabled ? 0.45 : 1
      }}
    >
      {props.children}
    </Text>
    </View>
  </Button>
);

export default Link;
