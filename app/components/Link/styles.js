import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  badgeContainer: {
    padding: 0,
    color: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  badgeStyle: {
    backgroundColor: '#FDDA24',
    borderRadius: 50,
    marginRight: 8
  },
  linkIcon: {
      fontSize: 14
  }
});
