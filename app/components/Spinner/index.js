/* @flow */
import * as React from "react";
import { View } from "react-native";
import { Spinner } from "native-base";

import styles from "./styles";

type Props = {
  condition: boolean
};

const Loader: React.StatelessFunctionalComponent<Props> = ({ condition }) =>
  condition ? (
    <View style={styles.spinnerContainer}>
      <Spinner color="#fff" />
    </View>
  ) : null;

export default Loader