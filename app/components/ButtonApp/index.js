/* @flow */
import * as React from 'react';
import { Button, Text } from 'native-base';

import styles from './styles';

const ButtonApp: React.StatelessFunctionalComponent<{
  style?: Object,
  textCustomStyle?: Object,
  children: React.Node
}> = props => (
  <Button style={{ ...styles.mainButton, ...props.style }} {...props}>
    <Text style={{ ...styles.textStyle, ...props.textCustomStyle }}>
      {props.children}
    </Text>
  </Button>
);

export default ButtonApp;
