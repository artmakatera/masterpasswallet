import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  mainButton: {
    alignItems: 'center',
    paddingHorizontal: 20,
    borderRadius: 50,
    paddingVertical: 14
  },
  textStyle: {
    textAlign: 'center',
    textAlignVertical: 'center',
    width: '100%'
  }
});
