/* @flow */
import * as React from 'react';
import { View } from 'native-base';
import Image from 'react-native-remote-svg';

import styles from './styles';

const MasterCardImages: React.StatelessFunctionalComponent<{}> = props => (
  <View style={styles.masterCardContainer}>
    <Image
      style={styles.imageStyle}
      source={require('../../assets/images/masterLogo.svg')}
    />
  </View>
);

export default MasterCardImages;
