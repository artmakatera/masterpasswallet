import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  masterCardContainer: {
    alignItems: 'center'
  },
  imageStyle: {
    width: 176,
    height: 20
  }
});
