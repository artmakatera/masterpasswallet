import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
});
