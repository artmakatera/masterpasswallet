/* @flow */
import * as React from 'react';
import { ImageBackground } from 'react-native';

import styles from './styles';

const BackgroundContainer: React.StatelessFunctionalComponent<{
  children: React.Node
}> = ({ children }) => {
  return (
    <ImageBackground
      source={require('../../assets/images/background.png')}
      style={styles.container}
    >
      {children}
    </ImageBackground>
  );
};

export default BackgroundContainer;
