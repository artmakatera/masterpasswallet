import { StyleSheet } from "react-native";
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window')

export default StyleSheet.create({
  messageContainers: {
    position: 'absolute',
    left: 0,
    height: 30,
    width,
    backgroundColor: '#E2575B',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
