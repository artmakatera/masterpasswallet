/* @flow */
import * as React from 'react';
import { Animated } from 'react-native';

import TextApp from '../TextApp';

import styles from './styles';

type ErrorMessageProps = {
  children: React.Node
};

type ErrorMessageState = {
  top: any
};
class ErrorMessage extends React.PureComponent<
  ErrorMessageProps,
  ErrorMessageState
> {
  state = {
    top: new Animated.Value(-100)
  };

  componentDidMount = () => {
    Animated.timing(this.state.top, {
      toValue: 30,
      duration: 1000
    }).start();
  };

  render() {
    const { top } = this.state;
    return (
      <Animated.View style={{ ...styles.messageContainers, top }}>
        <TextApp>{this.props.children}</TextApp>
      </Animated.View>
    );
  }
}

export default ErrorMessage;
