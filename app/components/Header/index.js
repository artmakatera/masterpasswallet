/* @flow */
import React, { PureComponent } from 'react';
import { Icon, View } from 'native-base';
import { withNavigation } from 'react-navigation';

import TextApp from '../TextApp';
import styles from './styles';

type HeaderProps = {
  navigation: any
};

class Header extends PureComponent<HeaderProps> {
  onBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View>
        <Icon
          type="MaterialIcons"
          name="arrow-back"
          style={styles.iconStyle}
          onPress={this.onBack}
        />
        <TextApp style={styles.headerText}>
          Підключення Masterpass-гаманця
        </TextApp>
      </View>
    );
  }
}

export default withNavigation(Header);
