import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  headerText: {
    fontSize: 24,
    fontWeight: '700',
    paddingBottom: 20
  },
  iconStyle: {
    color: 'white',
    fontSize: 18,
    paddingBottom: 22
  }
});
