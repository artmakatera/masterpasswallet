/* @flow */
import React, { Component } from 'react';

import { View } from 'native-base';
import { Keyboard, Animated, Dimensions, Easing } from 'react-native';

import BackgroundContainer from '../../components/BackgroundContainer';
import Header from '../../components/Header';
import TextApp from '../../components/TextApp';
import Link from '../../components/Link';
import Input from '../../components/Input';
import MasterCardImages from '../../components/MasterCardImages';
import ButtonApp from '../../components/ButtonApp';
import ModalWindow from '../../components/ModalWindow';
import ErrorMessage from '../../components/ErrorMessage';
import Timer from '../../components/Timer';
import { LoaderContext } from '../../context';

import request from '../../utils/api';

import styles from './styles';
import { resolve } from 'uri-js';

const BUTTON_HEIGHT = 45;
const TIMER_REMAIN = 3 * 1000 * 60;

type Props = {};
type State = {
  inputText: string,
  top: any,
  buttonWidth: any,
  disabled: boolean,
  modalIsShown: boolean,
  error: boolean,
  allowanceResend: boolean,
  showTimer: boolean
};

const { width: SCREEN_WIDTH } = Dimensions.get('screen');

class SMSCodeScreen extends Component<Props, State> {
  state = {
    inputText: '',
    top: new Animated.Value(-50),
    buttonWidth: new Animated.Value(150),
    disabled: true,
    modalIsShown: false,
    error: false,
    allowanceResend: false,
    showTimer: false
  };
  keyboardDidShowListener: any;
  keyboardDidHideListener: any;
  timeoutId: any;

  keyboardDidShow = (e: any) => {
    const { screenY } = e.endCoordinates;

    Animated.sequence([
      Animated.timing(this.state.top, {
        toValue: screenY - BUTTON_HEIGHT,
        duration: 1800,
        easing: Easing.linear
      }),
      Animated.timing(this.state.buttonWidth, {
        toValue: SCREEN_WIDTH,
        duration: 1000
      })
    ]).start();
  };

  componentDidMount = () => {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow
    );
    this.startTimer();
  };

  componentWillUnmount = () => {
    this.keyboardDidShowListener.remove();
    clearTimeout(this.timeoutId);
  };

  onChangeText = (text: string) => {
    this.setState(state => ({
      inputText: text,
      error: false,
      disabled: text ? false : true
    }));
  };

  onSubmit = async (toggleLoading: Function) => {
    try {
      toggleLoading(true);
      const { data } = await request.post('/login', {
        email: 'react@native.facebook.com',
        password: this.state.inputText
      });
      this.setState({ modalIsShown: true });
    } catch (e) {
      this.setState({ error: true });
    } finally {
      toggleLoading(false);
    }
  };

  closeModal = () => {
    this.setState({ modalIsShown: false });
  };

  onTimeElapsed = () => {
    this.setState({ allowanceResend: true, showTimer: false });
  };

  delay = (timeout: number) =>
    new Promise(resolve => {
      this.timeoutId = setTimeout(resolve, timeout);
    });

  startTimer = async () => {
    await this.delay(5000);
    this.setState({ showTimer: true });
  };

  resendSMS = () => {
    this.setState({ allowanceResend: false }, () => {
      this.startTimer();
    });
  };

  render() {
    const {
      allowanceResend,
      modalIsShown,
      top,
      buttonWidth,
      disabled,
      error,
      showTimer
    } = this.state;
    return (
      <LoaderContext.Consumer>
        {({ toggleLoading }) => (
          <BackgroundContainer>
            <Header />
            <TextApp>
              Щоб впевнитися що ви особисто підключаєте Masterpass-гаманець, ми
              тимчасово заблокуємо 1 гривню на картці із цього гаманця. Після
              цього вам надійде СМС з кодом підтвердження (VCODE) на той номер,
              який ви вказали у банку під час отримання картки.
            </TextApp>
            <View style={styles.topContainer}>
              <Input
                placeholder="Введіть VCODE c SMS"
                onChangeText={this.onChangeText}
                value={this.state.inputText}
              />
              <View style={styles.buttonContainer}>
                <View style={styles.paddingButton}>
                  <Link disabled={!allowanceResend} onPress={this.resendSMS}>
                    Не приходить SMS
                  </Link>
                </View>
                {showTimer ? (
                  <Timer
                    onTimeElapsed={this.onTimeElapsed}
                    initialSecondsRemaining={TIMER_REMAIN}
                  />
                ) : null}
              </View>
            </View>
            <MasterCardImages />
            <Animated.View
              style={{
                ...styles.submitButtonContainer,
                top
              }}
            >
              <Animated.View style={{ width: buttonWidth }}>
                <ButtonApp
                  warning
                  disabled={disabled}
                  style={{
                    alignSelf: 'center',
                    padding: 0,
                    alignItems: 'center',
                    width: '100%',
                    height: BUTTON_HEIGHT
                  }}
                  onPress={() => this.onSubmit(toggleLoading)}
                >
                  Підключити
                </ButtonApp>
              </Animated.View>
            </Animated.View>
            <ModalWindow
              isVisible={modalIsShown}
              closeModal={this.closeModal}
            />
            {error && (
              <ErrorMessage>
                Введено не вірний VCODE. Спробуйте знову.
              </ErrorMessage>
            )}
          </BackgroundContainer>
        )}
      </LoaderContext.Consumer>
    );
  }
}

export default SMSCodeScreen;
