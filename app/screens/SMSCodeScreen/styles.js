import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  screenContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
     paddingTop: 80,
     paddingBottom: 40,
     paddingHorizontal: 20
  },
  topContainer: {
    paddingTop: 35,
    flexGrow: 1
  },
  submitButtonContainer: {
    position: 'absolute',
    left: 0,
    width,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    paddingTop: 18,
    marginBottom: 60
  },
  paddingButton: {
    paddingRight: 20
  }
});
