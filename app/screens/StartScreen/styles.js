import { StyleSheet } from 'react-native';



export default StyleSheet.create({
  screenContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 80,
    paddingBottom: 40,
    paddingHorizontal: 20
  },
  topContainer: {
    flexGrow: 1
  },

  buttonConatiner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: 60
  },
  buttonStyle: {
    width: 158,
    borderRadius: 50
  }
});
