/* @flow */
import React, { Component } from 'react';

import { View } from 'native-base';

import { LoaderContext } from '../../context';
import request from '../../utils/api';

import TextApp from '../../components/TextApp';
import BackgroundContainer from '../../components/BackgroundContainer';
import ButtonApp from '../../components/ButtonApp';
import Link from '../../components/Link';
import MasterCardImages from '../../components/MasterCardImages';
import ErrorMessage from '../../components/ErrorMessage';
import Header from '../../components/Header';

import styles from './styles';

type StartScreenProps = {
  navigation: any
};

type StartScreenState = {
  error: boolean
};

class StartScreen extends Component<StartScreenProps, StartScreenState> {
  state = {
    error: false
  };

  onSubmit = async (toggleLoading: Function) => {
    try {
      toggleLoading(true);
      await request.post('/register', {
        email: 'react@native.facebook.com',
        password: 'pistol'
      });
      this.props.navigation.push('SmsScreen');
    } catch (e) {
    } finally {
      toggleLoading(false);
    }
  };
  render() {
    return (
      <LoaderContext.Consumer>
        {({ toggleLoading }) => (
          <BackgroundContainer>
            <Header />
            <TextApp style={styles.topContainer}>
              За номером{' '}
              <TextApp style={{ fontWeight: '700' }}>067 220 56 18</TextApp> вже
              є гаманець! Залишилось з’єднати його з додатком Мій Київстар, щоб
              поповнити рахунок.
            </TextApp>
            <View style={styles.buttonConatiner}>
              <Link>Пізніше</Link>
              <ButtonApp style={styles.buttonStyle} warning onPress={() => this.onSubmit(toggleLoading)}>
                З’єднати
              </ButtonApp>
            </View>
            {this.state.error && (
              <ErrorMessage>
                Проблеми зі зв'язком. Спробуйте знову.
              </ErrorMessage>
            )}
            <MasterCardImages />
          </BackgroundContainer>
        )}
      </LoaderContext.Consumer>
    );
  }
}

export default StartScreen;
